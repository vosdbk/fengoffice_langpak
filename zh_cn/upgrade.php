<?php

return array(
    /*
     * @translater terminus <terminus.ye@gmail.com>
     */
    'upgrade' => '更新',
    'upgrade from' => '更新自',
    'upgrade to' => '更新到',
    'upgrade from to' => '更新自 {0} 到 {1}',
    'already upgraded' => '已更新到最新版本: {0}',
    'back to fengoffice' => '返回系统',
    'all rights reserved' => '保留所有权利',
    'upgrade process log' => '更新日志',
    'upgrade fengoffice' => '更新系统',
    'upgrade your fengoffice installation' => '更新系统',
    'error upgrade version must be specified' => '未指定的版本。无法继续自动更新。请稍后再试或手动更新。',
    'error upgrade version not found' => '无效的版本（{0}）。无法继续自动更新。请稍后再试或手动更新。',
    'error upgrade invalid zip url' => '无效的版本更新地址。无法继续自动更新。请稍后再试或手动更新。',
    'error upgrade cannot open zip file' => '无法打开更新包。无法继续自动更新。请稍后再试或手动更新。',
    'upgrade warning' => '警告',
    'upgrade warning desc' => '请<a href="{0}" target="_blank">备份</ A>当前系统，再执行更新！',
);
?>
