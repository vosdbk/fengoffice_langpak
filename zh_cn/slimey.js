addLangs({
/*
 * @translater terminus <terminus.ye@gmail.com>
 */
    // functions.js
    "enter the url of the image": "图像位置",
    "enter a color": "颜色",
	
    // editor.js
    "drag the bottom right corner to resize": "拖动右下角调整大小",
    "double click to edit content": "双击编辑内容",
	
    // actions.js
    "some text": "文本",
	
    // navigation.js
    "new slide text": "编辑",
    "slide {0}": "幻灯片 {0}",
    "no slide to delete": "无法删除",
    "click top left button to add a slide": "点击左上角的按钮，添加幻灯片",
    "no slide to move": "无法移动",
    "click to insert a new slide": "点击新增幻灯片",
	
    // slimey.js
    "bold text": "粗体",
    "underline text": "下划线",
    "italic text": "斜体",
    "unsaved changes will be lost.": "未保存的更改会丢失。",
    "align text to the left": "左对齐",
    "align text to the center": "居中",
    "align text to the right": "右对齐",
	
    // tools.js
    "generic tool": "通用工具",
    "insert an element": "插入元素",
    "what element do you wish to insert?": "插入元素",
    "insert text": "文本",
    "insert image": "图像",
    "insert ordered list": "有序列表",
    "insert unordered list": "无序列表",
    "edit the elements content": "编辑元素内容",
    "change font color": "字体颜色",
    "change font size": "字体尺寸",
    "size": "尺寸",
    "font family": "字体",
    "generic fonts": "通用字体",
    "specific fonts": "特殊字体",
    "delete element": "删除元素",
    "redo": "重做",
    "undo": "后退",
    "send element to the back": "后置",
    "bring element to the front": "前置",
    "view source code": "源码",
    "save slideshow": "保存幻灯片",
    "preview slideshow": "预览",
    "add a new slide after the selected one": "新增幻灯片",
    "delete the selected slide": "删除幻灯片",
    "move the selected slide down one place": "后移",
    "move the selected slide up one place": "前移",
	
    // slimey.html
    "new slideshow": "新幻灯片",
    "second slide": "第二张"
});

