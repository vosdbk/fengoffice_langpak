<?php

/*
 * @translater terminus <terminus.ye@gmail.com>
 */
// Return array of langs
return array(
    'chelp pending tasks widget' => '此部件显示您未完成的任务. 您可以查看分配给您但仍未完成的任务. 您可以在控制面板中更改此设置.',
    'chelp documents widget' => '此部件显示选定工作区中最新的文件.',
    'chelp active tasks widget' => '此部件显示您活动与暂停的任务.',
    'chelp late tasks widget' => '此部件会将您今天到期及超期的任务和里程碑按截止日期排列显示..',
    'chelp calendar widget' => '此部件显示本周事件,到期任务及里程碑. 您可以点击某一天来创建新事件.',
    'chelp comments widget' => '此部件显示当前工作区对象的最新评论.',
    'chelp dashboard info widget' => '此部件显示当前工作区信息, 包括用户访问, 分配的联系人等.',
    'chelp emails widget' => '此部件显示当前工作区中已分类的最新邮件.',
    'chelp messages widget' => '此部件显示最新的笔记.',
    'chelp active tasks panel' => '显示所有活动的任务并提供任务暂停, 继续或完成任务的控制面板.',
    'chelp general timeslots panel' => '此面板显示直接分配到工作区的时隙.',
    'chelp personal account' => '这是您的个人帐户.<br /> 在此视图中, 您可以更新个人资料和头像, 修改密码以及编辑个人喜好.',
    'chelp user account' => '这是一个用户帐户.',
    'chelp user account admin' => '这是一个用户帐户.<br /> 在此视图中, 您可以更新用户的个人资料和头像, 修改密码以及编辑个人喜好.',
    'chelp reporting panel' => '欢迎访问报表面板.<br /> 您可以从左侧面板中从不同对象中自定义报表.',
    'chelp reporting panel manage' => '欢迎访问报表面板.<br /> 您可以从左侧面板中从不同对象中自定义报表.<br /> 您也有管理报表的权限, 可以创建和管理每个对象类型的报表. 这些在用户权限部分设置.',
    'chelp reporting panel manage admin' => '欢迎访问报表面板.<br /> 您可以从左侧面板中从不同对象中自定义报表.<br /> 您也有管理报表的权限, 可以创建和管理每个对象类型的报表. 这些在用户权限部分设置.',
    'chelp addfile' => '您可以选择上传文件或者外部链接.<br />
						<b>文件:</b> 从本地计算机上传任何文件.<br/>
						<b>外部链接:</b> 输入一个想链接到的URL地址.<br/>',
    'remove context help' => '移除此帮助信息',
    'chelp tasks list welcome' => '欢迎访问任务列表面板',
    'chelp tasks list' => '<br/>您可以使用以下的过滤器:<br/>
							<b>创建者</b>: 仅显示指定用户或组创建的任务 <br />
							<b>完成者</b>: 仅显示指定用户或组完成的任务 <br />
							<b>分配到</b>: 仅显示分配到指定用户或组的任务 <br />
							<b>分配者</b>: 仅显示指定用户或组分配的任务 <br />
							<b>里程碑</b>: 仅显示属于指定里程碑的任务<br />
							<b>优先级</b>: 仅显示指定优先级的任务 (低, 普通, 高) <br />
							</td>
							<td>
							<br/>您可以把任务按以下条件分组:<br />
							<b>里程碑</b>: 将任务分组到指定的里程碑 <br/>
							<b>优先级</b>: 将任务分组到指定的优先级 (低, 普通, 高) <br />
							<b>工作区</b>: 将任务分组到指定的工作区 <br />',
    'help opt show' => '显示',
    'help opt hide' => '隐藏',
    'context help messages' => '上下文帮助信息',
    'enable all chelp messages' => '启用所有手动关闭的上下文消息',
    'enable all chelp messages click' => '点击这里',
    'help manual' => '帮助手册',
    'about us' => '关于我们',
    'success enable all context help' => '重新打开上下文帮助成功.',
    'success enable context help' => '上下文帮助启用成功.',
    'success disable context help' => '上下问帮助禁用成功.',
    'how to purchase' => '如何购买专业版',
    'how to purchase desc' => '关于专业版的不同与优点',
    'add ticket' => '投个支持票',
    'add ticket desc' => "获得系统的个性化帮助",
); // array
?>